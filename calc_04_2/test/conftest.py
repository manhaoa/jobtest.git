import pytest

from calc_04_2.code.calc import Calculator


@pytest.fixture(scope="module")
def init():
    print("开始计算")
    # calc1 = Calculator()
    yield
    print("计算结束")

@pytest.fixture(scope='class')
def get_calc():
    print("获取计算机实例1")
    calc = Calculator()
    return calc