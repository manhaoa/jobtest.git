import allure
import pytest
import yaml

with open("../data/data.yaml") as ff:
    add_data = yaml.safe_load(ff)['mul']

@pytest.fixture(params=add_data)
def get_data(request):
    print("开始计算")
    data=request.param
    print(f"测试数据为：{data}")
    yield data
    print("计算结束")


# @pytest.fixture(scope='class')
# def get_calc():
#     print("获取计算机实例")
#     calc = Calculator()
#     return calc

@allure.feature("测试计算器")
class TestCalc:
    # def setup(self):
    #     print("开始计算")
    #     self.calc1 = Calculator()
    # def teardown(self):
    #     print("计算结束")

    # @pytest.mark.parametrize(
    #     "a,b,c", add_data
    # )
    @allure.story("测试乘法")
    @pytest.mark.run(order=3)
    def test_mul(self,get_calc,get_data):
        with allure.step("计算两个数的相乘和"):
            result = get_calc.mul(get_data[0], get_data[1])
            if isinstance(result,float):
                result=round(result,2)
            assert result == get_data[2]



