import pytest
import yaml

from calc_04_2.code.calc import Calculator


class TestCalc:
    def setup(self):
        print("开始计算")
        self.calc1 = Calculator()
    def teardown(self):
        print("计算结束")
    with open("../data/data.yaml") as ff:
        add_data=yaml.safe_load(ff)['sub']
    @pytest.mark.parametrize(
        "a,b,c",add_data
    )
    @pytest.mark.run(order=2)
    def test_sub(self,a,b,c):
        result = self.calc1.sub(a, b)
        assert result == c



