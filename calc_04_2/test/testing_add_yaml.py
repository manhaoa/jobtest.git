import pytest

from calc_04_2.code.calc import Calculator


@pytest.fixture(autouse=True)
def init1():
    print("开始计算")
    yield
    print("计算结束")

@pytest.mark.run(order=1)
def test_add(init1):
        #实例化计算器类
    calc = Calculator()
    result = calc.add(1, 2)
    if isinstance(result,float):
        result =round(result,2)
    assert result == 3

def test_add1():

    # 实例化计算器类
    calc = Calculator()
    result = calc.add(-1, -2)
    if isinstance(result, float):
        result = round(result, 2)
    assert result == -3

def test_add2(init1):

    # 实例化计算器类
    calc = Calculator()
    result = calc.add(1, 2)
    if isinstance(result, float):
        result = round(result, 2)
    assert result == 3
# class TestCalc:
    # def setup(self):
    #     print("开始计算")
    #     self.calc1 = Calculator()
    # def teardown(self):
    #     print("计算结束")
    # with open("../data/data.yaml") as ff:
    #     add_data=yaml.safe_load(ff)['add']
    # @pytest.mark.parametrize(
    #     "a,b,c",add_data
    # )






