import pytest

from calc_04_1.code.calc import Calculator


class TestCalc:
    def setup(self):
        print("开始计算")
        self.calc1 = Calculator()
    def teardown(self):
        print("计算结束")
    # @pytest.mark.parametrize()

    def test_add(self):
        #实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(1, 2)
        assert result == 3
    def test_add1(self):
        #实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(110,200)
        assert result == 310
    def test_add2(self):
        #实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(1.9,2.7)
        assert result == 4.6
    def test_add3(self):
         # 实例化计算器类
       # calc = Calculator()
        result = self.calc1.add(-1.9, -2.7)
        assert result == -4.6
    def test_add4(self):
         # 实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(-1.0, -2.0)
        assert result == -3.0

    def test_add5(self):
         # 实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(-1.0, 2.0)
        assert result == 1.0

    def test_add6(self):
        # 实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(1.0, 2.0)
        assert result == 3.0
    def test_add7(self):
        # 实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(0, 0)
        assert result == 0



    """
   被除数   除数    预期结果
    10      2          5   
     4.8    2.4       2
     6       -3          -2
    -6      -3          2
     4       0          报错
     0      1           0
      900     300      3
    
    """
    def test_div(self):
        result = self.calc1.div(10,2)
        assert result ==5
    def test_div1(self):
        result = self.calc1.div(4.8,2.4)
        assert result ==2
    def test_div2(self):
        result = self.calc1.div(6,-3)
        assert result ==-2
    def test_div3(self):
        result = self.calc1.div(-6,-3)
        assert result ==2
    # def test_div4(self):
    #     result = self.calc1.div(4,0)
    #     assert result =="报错"
    def test_div5(self):
        result = self.calc1.div(0,1)
        assert result ==0

