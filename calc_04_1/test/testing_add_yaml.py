import pytest
import yaml

from calc_04_1.code.calc import Calculator


class TestCalc:
    def setup(self):
        print("开始计算")
        self.calc1 = Calculator()
    def teardown(self):
        print("计算结束")
    with open("../data/add_data.yaml") as ff:
        add_data=yaml.safe_load(ff)['add1']
    @pytest.mark.parametrize(
        "a,b,c",add_data
    )


    def test_add(self,a,b,c):
        #实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(a, b)
        if isinstance(result,float):
            result =round(result,2)
        assert result == c


