import pytest

from calc_04_1.code.calc import Calculator


class TestCalc:
    def setup(self):
        print("开始计算")
        self.calc1 = Calculator()
    def teardown(self):
        print("计算结束")
    @pytest.mark.parametrize(
        "a,b,c",
        [
            (1,2,3),
            (110,200,310),
            (1.9, 2.7, 4.6),
            (-1.9, -2.7, -4.6),
            (-1.0, -2.0, -3.0),
            (-1.0, 2.0, 1.0),
            (1.0,2.0,3.0),
            (0,0,0),
            (0.1,0.2,0.3)
        ]
    )


    def test_add(self,a,b,c):
        #实例化计算器类
        #calc = Calculator()
        result = self.calc1.add(a, b)
        if isinstance(result,float):
            result =round(result,2)
        assert result == c


