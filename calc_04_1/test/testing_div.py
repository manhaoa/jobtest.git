import pytest

from calc_04_1.code.calc import Calculator


class TestCalc:
    def setup(self):
        print("开始计算")
        self.calc1 = Calculator()
    def teardown(self):
        print("计算结束")



    @pytest.mark.parametrize(
        "a,b,c",
        [
            (10,2, 5),
            (4.8,2.4, 2),
            (6,-3, -2),
            (-6,-3, 2),
            (0, 1, 0)
        ]
    )
    def test_div(self,a,b,c):
        result = self.calc1.div(a,b)
        assert result ==c



