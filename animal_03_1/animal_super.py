class Animal:
    def __init__(self,name,color,age,sex):
        self.name = name
        self.color = color
        self.age = age
        self.sex = sex
    def say(self):
        print(self.name+"说话")
    def run(self):
        print(self.name+"跑")
class Cat(Animal):
    def __init__(self,name,color,age,sex):
        super().__init__(name,color,age,sex)
        self.hair = "短毛"
    def catch(self):
        print(self.name+"会捉老鼠")
    def say(self):
        print(self.name+"喵喵叫")
if __name__ == '__main__':
    cat = Cat("Kitty","白色",1,"公猫")
    cat.catch()
    cat.say()