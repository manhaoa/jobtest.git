from money import saved_money
from select_money import show_money
from send_money import add_money

print("存款前")
show_money(saved_money)
print("存款后:")
money = add_money(999)
show_money(money)